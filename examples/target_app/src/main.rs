// @Author: Lashermes Ronan <ronan>
// @Date:   24-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 26-07-2017
// @License: MIT

extern crate libmaj;
extern crate base64;

use base64::decode;
use libmaj::Updater;

const ENDPOINT: &str = "https://gitlab.com/Artefaritaj/maj/raw/master/examples/target_app/versions.json";
const PUB_KEY_B64: &str = "EXeczPQRpACoNE5Q1mnQeay2ZOK7RyW3DNEnElCO3ho=";
const UPDATE_POLICY: &str = "~0";
const THIS_VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    println!("Shall we play a game?");
    println!("This is version {}", THIS_VERSION);

    if let Err(e) = run_self_update() {
        println!("Application error: {}", e.to_string());
    }
}

fn run_self_update() -> Result<(), String> {
    println!("Is there a new version of myself?");

    let pub_key = decode(PUB_KEY_B64).map_err(|e|e.to_string())?;
    let updated_version_opt = Updater::update_str(ENDPOINT, &pub_key, THIS_VERSION, UPDATE_POLICY).map_err(|e|e.to_string())?;
    if let Some(updated_version) = updated_version_opt {
        println!("This app has been updated to version {}. Restart to see the changes.", updated_version);
    }
    else {
        println!("This app is already up-to-date.");
    }

    Ok(())
}

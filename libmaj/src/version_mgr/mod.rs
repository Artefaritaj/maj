// @Author: Lashermes Ronan <ronan>
// @Date:   24-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 24-07-2017
// @License: MIT

pub mod version_manager;
pub mod app_version_entry;

pub use self::version_manager::VersionManager;
pub use self::app_version_entry::AppVersionEntry;

// @Author: Lashermes Ronan <ronan>
// @Date:   24-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 25-07-2017
// @License: MIT

use semver::Version;
use base64::{encode, decode};

use crate::key_mgr::KeyFile;

use std::cmp::{PartialOrd, Ord, Ordering};
use std::str::FromStr;

#[derive(Debug,Clone)]
pub struct AppVersionEntry {
    version: Version,
    url_path: String,
    signature: String
}

#[derive(Debug,Clone,Serialize,Deserialize)]
pub struct AppVersionEntrySer {
    version: String,
    url_path: String,
    signature: String
}

impl AppVersionEntry {
    pub fn new(version: Version, url_path: String, key_file: &KeyFile) -> Result<AppVersionEntry, String> {
        let to_sign = version.to_string() + "||" + &url_path;
        let signature = encode(&key_file.pk_sign(to_sign.as_bytes())?);

        Ok(AppVersionEntry{ version, url_path, signature})
    }

    pub fn verify(&self, public_key_bytes: &[u8]) -> Result<bool, String> {
        let to_verify = self.version.to_string() + "||" + &self.url_path;
        let signature = decode(&self.signature).map_err(|_|format!("Cannot decode signature."))?;
        let is_verified = KeyFile::pk_verify(public_key_bytes, &to_verify.as_bytes(), &signature)?;

        Ok(is_verified)
    }

    pub fn get_version(&self) -> &Version {
        &self.version
    }

    pub fn get_url(&self) -> &str {
        &self.url_path
    }

    pub fn to_serializable(&self) -> AppVersionEntrySer {
        AppVersionEntrySer { version: self.version.to_string(), url_path: self.url_path.clone(), signature: self.signature.clone() }
    }
}

impl PartialOrd for AppVersionEntry {
    fn partial_cmp(&self, other: &AppVersionEntry) -> Option<Ordering> {
        Some(self.version.cmp(&other.version))
    }
}

impl Ord for AppVersionEntry {
    fn cmp(&self, other: &AppVersionEntry) -> Ordering {
        self.version.cmp(&other.version)
    }
}

impl PartialEq for AppVersionEntry {
    fn eq(&self, other: &AppVersionEntry) -> bool {
        self.version == other.version
    }
}

impl Eq for AppVersionEntry {}

impl AppVersionEntrySer {
    pub fn to_app_version_entry(self) -> Result<AppVersionEntry, String> {
        Ok(AppVersionEntry { version: Version::from_str(&self.version).map_err(|e|e.to_string())?,
            url_path: self.url_path,
            signature: self.signature })
    }
}

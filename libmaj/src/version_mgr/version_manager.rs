// @Author: Lashermes Ronan <ronan>
// @Date:   24-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 26-07-2017
// @License: MIT

use std::str::FromStr;
use std::io::{Read, BufWriter, BufReader};
use std::fs::File;
use std::path::Path;

use semver::{Version, VersionReq};
use serde_json;
use crate::Url;
use file_fetcher;

use crate::version_mgr::{
    AppVersionEntry,
    app_version_entry::AppVersionEntrySer
};
use crate::key_mgr::KeyFile;
use crate::errors::*;

///The version manager help to choose a version according to a update policy
//and link to the url where you can find the corresponding package.
#[derive(Debug,Clone)]
pub struct VersionManager {
    app_name: String,

    ///List of available versions
    app_versions: Vec<AppVersionEntry>
}

impl VersionManager {
    pub fn new(app_name: String) -> VersionManager {
        VersionManager { app_name, app_versions: Vec::new() }
    }

    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<VersionManager> {
        if path.as_ref().exists() {
            let file = File::open(path)?;
            let reader = BufReader::new(file);
            VersionManager::from_reader(reader)
        }
        else {
            Err(format!("File {} does not exist.", path.as_ref().display()).into())
        }
    }

    pub fn from_url_str(url: &str) -> Result<VersionManager> {
        let url = Url::parse(url)?;
        VersionManager::from_url(url)
    }

    pub fn from_url(url: Url) -> Result<VersionManager> {
        let deser: VersionManagerSer = file_fetcher::open_json(url)?;
        deser.to_version_manager()
    }

    pub fn from_reader<T: Read>(reader: T) -> Result<VersionManager> {
        let deser: VersionManagerSer = serde_json::from_reader(reader)?;
        deser.to_version_manager()
    }

    pub fn save_to_file<P: AsRef<Path>>(&self, path: P) -> Result<()> {
        let ser = self.to_serializable();
        let file = File::create(path)?;
        let writer = BufWriter::new(file);
        serde_json::to_writer(writer, &ser)?;
        Ok(())
    }

    pub fn to_json(&self) -> Result<String> {
        let ser = self.to_serializable();
        Ok(serde_json::to_string(&ser)?)
    }

    pub fn add_entry(&mut self, entry: AppVersionEntry) {
        self.app_versions.push(entry);
    }

    pub fn add_app_version(&mut self, version: Version, url: String, key_file: &KeyFile) -> Result<()> {
        self.app_versions.push(AppVersionEntry::new(version, url, key_file)?);
        Ok(())
    }

    pub fn add_app_version_str(&mut self, version_str: &str, url: String, key_file: &KeyFile) -> Result<()> {
        let version = Version::from_str(version_str).map_err(|e|e.to_string())?;
        self.app_versions.push(AppVersionEntry::new(version, url, key_file)?);
        Ok(())
    }

    pub fn remove_version(&mut self, version: &Version) -> Result<()> {
        let index = self.app_versions.iter().position(|ref entry| entry.get_version() == version).ok_or(format!("Cannot find version {}", version))?;
        self.app_versions.remove(index);
        Ok(())
    }

    pub fn remove_version_str(&mut self, version_str: &str) -> Result<()> {
        let version = Version::from_str(version_str)?;
        self.remove_version(&version)
    }

    pub fn verify(&self, public_key_bytes: &[u8]) -> Result<bool> {
        for entry in self.app_versions.iter() {
            let is_entry_verified = entry.verify(public_key_bytes)?;
            if is_entry_verified == false {
                return Ok(false);
            }
        }
        Ok(true)
    }

    pub fn get_last_version_url(&self) -> Result<AppVersionEntry> {
        let mut sorted = self.app_versions.clone();
        sorted.sort();
        let last_entry = sorted.last().ok_or(format!("Cannot find the last version."))?;
        Ok(last_entry.clone())
    }

    pub fn get_matching_versions(&self, version_request: &VersionReq) -> Vec<AppVersionEntry> {
        let mut matching: Vec<AppVersionEntry> = Vec::new();
        for entry in self.app_versions.iter() {
            if version_request.matches(entry.get_version()) {
                matching.push(entry.clone());
            }
        }
        matching
    }

    pub fn get_last_matching_version(&self, version_request: &VersionReq) -> Option<AppVersionEntry> {
        let mut matching = self.get_matching_versions(version_request);
        matching.sort();
        if let Some(last_entry) = matching.last() {
            Some(last_entry.clone())
        }
        else {
            None
        }
    }

    pub fn get_last_matching_version_str(&self, version_request_str: &str) -> Result<AppVersionEntry> {
        let version_request = VersionReq::from_str(version_request_str).map_err(|e|e.to_string())?;
        let mut matching = self.get_matching_versions(&version_request);
        matching.sort();
        let last_entry = matching.last().ok_or(format!("Cannot find the last version."))?;
        Ok(last_entry.clone())
    }

    /// Get the new package url if there is an update matching update policy
    pub fn get_update_url(&self, this_version: &Version, update_policy: &VersionReq) -> Option<String> {
        if let Some(last_entry) = self.get_last_matching_version(update_policy) {
            if last_entry.get_version() > this_version {
                Some(last_entry.get_url().to_string())
            }
            else {
                None
            }
        }
        else {//No update
            None
        }
    }

    /// Get the new package url if there is an update matching update policy
    pub fn get_update_url_str(&self, this_version: &str, update_policy: &str) -> Result<Option<String>> {
        let this_version = Version::parse(this_version)?;
        let update_policy = VersionReq::parse(update_policy)?;
        if let Some(last_entry) = self.get_last_matching_version(&update_policy) {
            if last_entry.get_version() > &this_version {
                Ok(Some(last_entry.get_url().to_string()))
            }
            else {
                Ok(None)
            }
        }
        else {//No update
            Ok(None)
        }
    }

    pub fn get_app_name(&self) -> &str {
        &self.app_name
    }
    fn to_serializable(&self) -> VersionManagerSer {
        let mut vmgr_ser = VersionManagerSer { app_name: self.get_app_name().to_string(), entries: Vec::new() };

        for entry in self.app_versions.iter() {
            vmgr_ser.entries.push(entry.to_serializable());
        }

        vmgr_ser
    }
}

#[derive(Debug,Clone,Serialize,Deserialize)]
struct VersionManagerSer {
    app_name: String,
    entries: Vec<AppVersionEntrySer>
}

impl VersionManagerSer {
    pub fn to_version_manager(mut self) -> Result<VersionManager> {
        let mut vmgr = VersionManager::new(self.app_name);
        while let Some(entry) = self.entries.pop() {
            vmgr.add_entry(entry.to_app_version_entry()?);
        }
        Ok(vmgr)
    }

}

#[test]
fn test_version_manager() {
    let mut vmgr = VersionManager::new("test".to_string());
    let key_file = KeyFile::new().unwrap();

    vmgr.add_app_version(Version::from_str("0.1.2").unwrap(), "url1".to_string(), &key_file).unwrap();
    vmgr.add_app_version(Version::from_str("2.1.3").unwrap(), "url2".to_string(), &key_file).unwrap();
    vmgr.add_app_version(Version::from_str("0.2.0").unwrap(), "url3".to_string(), &key_file).unwrap();
    vmgr.add_app_version(Version::from_str("1.1.0").unwrap(), "url4".to_string(), &key_file).unwrap();

    assert!(vmgr.verify(&key_file.get_pk_verification_key().unwrap()).unwrap()==true);


    let ver_req = VersionReq::from_str(">= 0.1.3").unwrap();
    let _ = vmgr.get_matching_versions(&ver_req);
    // println!("Versions matching \"{}\"", ver_req);
    // for entry in matching.iter() {
    //      println!("{} -> {}", entry.get_version(), entry.get_url());
    // }

    let entry = vmgr.get_last_version_url().unwrap();
    // println!("Last: {} -> {}", entry.get_version(), entry.get_url());
    assert!(entry.get_version().to_string() == "2.1.3");

    let ver_req = VersionReq::from_str("~0").unwrap();
    let entry = vmgr.get_last_matching_version(&ver_req).unwrap();
    assert!(entry.get_version().to_string() == "0.2.0");
    // println!("Last matching {}: {} -> {}", ver_req, entry.get_version(), entry.get_url());
}

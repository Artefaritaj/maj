// @Author: Lashermes Ronan <ronan>
// @Date:   26-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 26-07-2017
// @License: MIT



extern crate xdg;
use crate::app_dirs::common::*;
use self::xdg::BaseDirectories as Xdg;
use std::path::PathBuf;

pub const USE_AUTHOR: bool = false;

pub fn get_app_dir(t: AppDataType) -> Result<PathBuf, AppDirsError> {
    Xdg::new()
        .ok()
        .as_ref()
        .and_then(|x| match t {
            AppDataType::UserConfig => Some(x.get_config_home()),
            AppDataType::UserData => Some(x.get_data_home()),
            AppDataType::UserCache => Some(x.get_cache_home()),
            AppDataType::SharedData => x.get_data_dirs().into_iter().next(),
            AppDataType::SharedConfig => x.get_config_dirs().into_iter().next(),
        })
        .ok_or_else(|| AppDirsError::NotSupported)
}

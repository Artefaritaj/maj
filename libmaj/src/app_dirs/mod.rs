// @Author: Lashermes Ronan <ronan>
// @Date:   26-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 26-07-2017
// @License: MIT

//Embed app_dirs fork from https://github.com/TruputiGalvotas/app-dirs-rs.git so has to publish crate
//Remove this when app_dirs merge pull request or cargo allows to publish with patched dependencies

mod common;
pub use self::common::*;
mod imp;
pub use self::imp::*;
mod utils;
pub use self::utils::*;

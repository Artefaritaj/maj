// @Author: Lashermes Ronan <ronan>
// @Date:   17-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 24-07-2017
// @License: MIT

use ring::{hmac, rand};
use ring::rand::SecureRandom;
use ring::hmac::Key;

use std::fs;
use std::io::{BufReader, Read};
use std::path::Path;

use crate::key_mgr::constants::*;

const BUF_SIZE: usize = 8192;

///Sign data with hmac key. Ensure integrity,
pub fn hmac_sign(key_bytes: &[u8], to_sign: &[u8]) -> Vec<u8> {
    let key = hmac::Key::new(hmac::HMAC_SHA256, key_bytes);
    let signature = hmac::sign(&key, to_sign);
    signature.as_ref().to_vec()
}

///Verify hmac
pub fn hmac_verify(key: &Key, data_to_verify: &[u8], data_signature: &[u8]) -> bool {
    match hmac::verify(&key, data_to_verify, data_signature).map_err(|e|e.to_string()) {
        Ok(_) => true,
        Err(_) => false
    }
}

///Sign big file with hmac key. Ensure integrity.
pub fn hmac_sign_file<T: AsRef<Path>>(key_bytes: &[u8], path: T) -> Result<Vec<u8>, String> {
    if path.as_ref().exists() {
        let file = fs::File::open(path).map_err(|e|e.to_string())?;
        let mut buf_read = BufReader::with_capacity(BUF_SIZE, file);
        let mut buffer = [0u8; BUF_SIZE];
        let key = hmac::Key::new(hmac::HMAC_SHA256, key_bytes);
        let mut ctx = hmac::Context::with_key(&key);

        loop {
            match buf_read.read(&mut buffer) {
                Ok(read_count) => {
                    ctx.update(&buffer);

                    if read_count == 0 {
                        break;
                    }
                },
                Err(e) => { return Err(e.to_string()); }
            }
        }

        Ok(ctx.sign().as_ref().to_vec())
    }
    else {
        Err(format!("File cannot be signed: it does not exist."))
    }
}

pub fn hmac_verify_file<T: AsRef<Path>>(key: &Key, path: T, file_signature: &[u8]) -> Result<bool, String> {
    if path.as_ref().exists() {
        let file = fs::File::open(path).map_err(|e|e.to_string())?;
        let mut buf_read = BufReader::with_capacity(BUF_SIZE, file);
        let mut buffer = [0u8; BUF_SIZE];
        let mut msg = Vec::<u8>::new();

        loop {
            match buf_read.read(&mut buffer) {
                Ok(read_count) => {
                    msg.extend(buffer.iter());

                    if read_count == 0 {
                        break;
                    }
                },
                Err(e) => { return Err(e.to_string()); }
            }
        }

        match hmac::verify(&key, &msg, file_signature).map_err(|e|e.to_string()) {
            Ok(_) => Ok(true),
            Err(_) => Ok(false)
        }
    }
    else {
        Err(format!("File cannot be verified: it does not exist."))
    }
}

///Get hmac key for integrity verification
pub fn generate_random_hmac_key() -> Result<Vec<u8>, String> {
    let rng = rand::SystemRandom::new();
    //Generate a hmac key
    let mut hmac_key = [0u8; PAYLOAD_SIZE];
    rng.fill(&mut hmac_key).map_err(|_|format!("Cannot fill random key."))?;

    Ok(hmac_key.to_vec())
}

pub fn derive_hmac_verification_key(hmac_key: &[u8]) -> Key {
    hmac::Key::new(hmac::HMAC_SHA256, hmac_key)
}

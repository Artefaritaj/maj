// @Author: Lashermes Ronan <ronan>
// @Date:   07-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 24-07-2017
// @License: MIT

use std::path::Path;
use std::fs::File;
use std::io::{BufReader, BufWriter};
use std::num::NonZeroU32;

use serde_json;

use crate::key_mgr::KeyFile;
use crate::key_mgr::constants::*;

use ring::{rand, pbkdf2, aead};
use ring::rand::SecureRandom;
use ring::aead::{SealingKey, OpeningKey, BoundKey};


#[derive(Debug,Clone,Serialize,Deserialize)]
pub struct EncryptedKeyFile {
    encrypted_payload: Vec<u8>,
    aead_nonce: [u8; NONCE_LEN],
    password_salt: Vec<u8>
}

impl EncryptedKeyFile {

    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<EncryptedKeyFile, String> {
        if path.as_ref().exists() {
            let file = File::open(path).map_err(|e|format!("Cannot open key file: {}", e))?;
            let reader = BufReader::new(file);
            let enc_key_file: EncryptedKeyFile = serde_json::from_reader(reader).map_err(|e|format!("Cannot parse JSON: {}",e))?;
            Ok(enc_key_file)
        }
        else {
            Err(format!("File {} does not exist.", path.as_ref().display()))
        }
    }

    pub fn save_to_file<P: AsRef<Path>>(&self, path: P) -> Result<(), String> {
        let file = File::create(path).map_err(|e|e.to_string())?;
        let writer = BufWriter::new(file);
        serde_json::to_writer(writer, &self).map_err(|e|e.to_string())?;
        Ok(())
    }

    fn derive_key(salt: &[u8], password: &str) -> Result<Credential, String> {
        //derive file key from password and salt
        let mut key_bytes: Credential = [0u8; CREDENTIAL_LEN];//store the key bytes here
        pbkdf2::derive(PBKDF2_ALG, 
                    NonZeroU32::new(PBKDF2_ITER_NB).ok_or(format!("Incorrect number of iteration"))?,
                    salt, 
                    password.as_bytes(),
                    &mut key_bytes);

        //check if key is correct size
        if key_bytes.len() != AEAD_ALG.key_len() {
            return Err(format!("Key len is {} instead of {}.", key_bytes.len(), AEAD_ALG.key_len()));
        }

        Ok(key_bytes)
    }


    fn derive_sealing_key(salt: &[u8], nonce: &[u8; NONCE_LEN], password: &str) -> Result<SealingKey<OneNonceSequence>, String> {
        let key_bytes = EncryptedKeyFile::derive_key(salt, password)?;
        let ukey = aead::UnboundKey::new(AEAD_ALG, &key_bytes).map_err(|_|format!("Cannot generate AEAD key."))?;
        let nonce_seq = OneNonceSequence::new(aead::Nonce::assume_unique_for_key(nonce.clone()));
        Ok(SealingKey::new(ukey, nonce_seq))
    }

    fn derive_opening_key(salt: &[u8], nonce: &[u8; NONCE_LEN], password: &str) -> Result<OpeningKey<OneNonceSequence>, String> {
        let key_bytes = EncryptedKeyFile::derive_key(salt, password)?;
        let ukey = aead::UnboundKey::new(AEAD_ALG, &key_bytes).map_err(|_|format!("Cannot generate AEAD key."))?;
        let nonce_seq = OneNonceSequence::new(aead::Nonce::assume_unique_for_key(nonce.clone()));
        Ok(OpeningKey::new(ukey, nonce_seq))
    }

    pub fn decrypt(self, password: &str) -> Result<KeyFile, String> {
        //get key
        let mut opening_key = EncryptedKeyFile::derive_opening_key(&self.password_salt, &self.aead_nonce, password)?;
        //read encrypted payload
        let mut file_buf: Vec<u8> = self.encrypted_payload;
        //decrypt
        let decrypted = opening_key.open_in_place(aead::Aad::from(AUTH.as_bytes()), &mut file_buf).map_err(|_|format!("Failed to decrypt data."))?;
        //deserialize decrypted payload
        let key_file: KeyFile = serde_json::from_slice(decrypted).map_err(|e|e.to_string())?;
        Ok(key_file)
    }


    pub fn encrypt(key_file: &KeyFile, password: &str) -> Result<EncryptedKeyFile, String> {
        let rng = rand::SystemRandom::new();
        //fill salt
        let mut salt: [u8; SALT_LEN] = [0u8;SALT_LEN];
        rng.fill(&mut salt).map_err(|_|format!("Cannot fill random salt."))?;

        //fill nonce
        let mut nonce: [u8; NONCE_LEN] = [0u8; NONCE_LEN];
        rng.fill(&mut nonce).map_err(|_|format!("Cannot fill random nonce."))?;

        //payload to encrypt is serialized key file
        let mut payload = serde_json::to_vec(key_file).map_err(|e|e.to_string())?;

        //get encryption key
        let mut sealing_key = EncryptedKeyFile::derive_sealing_key(&salt, &nonce, password).map_err(|e|format!("Cannot derive sealing key: {}", e))?;

        //encrypt payload
        sealing_key.seal_in_place_append_tag(aead::Aad::from(AUTH.as_bytes()), &mut payload).map_err(|e|format!("Encryption failure: {}", e))?;

        Ok(EncryptedKeyFile { encrypted_payload: payload, aead_nonce: nonce, password_salt: salt.to_vec() })
    }

}


struct OneNonceSequence(Option<aead::Nonce>);

impl OneNonceSequence {
    /// Constructs the sequence allowing `advance()` to be called
    /// `allowed_invocations` times.
    fn new(nonce: aead::Nonce) -> Self {
        Self(Some(nonce))
    }
}

impl aead::NonceSequence for OneNonceSequence {
    fn advance(&mut self) -> Result<aead::Nonce, ring::error::Unspecified> {
        self.0.take().ok_or(ring::error::Unspecified)
    }
}
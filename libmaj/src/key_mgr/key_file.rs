// @Author: Lashermes Ronan <ronan>
// @Date:   07-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 07-07-2017
// @License: MIT

use ring::{
    rand, 
    signature::{
        self,
        KeyPair,
    }
};

use std::path::Path;

use std::fs;
use std::io::{Read};

///The key file contains the (private) data to sign
///The key file data must be encrypted when at rest
///API is designed so that secret data do not leak
#[derive(Debug,Clone,Serialize,Deserialize)]
pub struct KeyFile {
    pkcs8_key_pair: Vec<u8>
}


impl KeyFile {
    ///Create a new key file.
    ///A pk (ECC) key pair.
    pub fn new() -> Result<KeyFile, String> {
        let rng = rand::SystemRandom::new();

        // Generate a key pair in PKCS#8 (v2) format.
        let pkcs8_bytes = signature::Ed25519KeyPair::generate_pkcs8(&rng).map_err(|_|format!("Failed to generate key pair."))?;

        Ok(KeyFile { pkcs8_key_pair: pkcs8_bytes.as_ref().to_vec()})
    }


    ///Sign data with a pk private key. Ensure authenticity, not integrity.
    pub fn pk_sign(&self, to_sign: &[u8]) -> Result<Vec<u8>, String> {
        let key_pair = signature::Ed25519KeyPair::from_pkcs8(&self.pkcs8_key_pair).map_err(|e|e.to_string())?;
        let signature = key_pair.sign(to_sign);
        Ok(signature.as_ref().to_vec())
    }

    pub fn pk_verify(public_key_bytes: &[u8], data_to_verify: &[u8], signature: &[u8]) -> Result<bool, String> {
        // let key_pair = signature::Ed25519KeyPair::from_pkcs8(untrusted::Input::from(&self.pkcs8_key_pair)).map_err(|e|e.to_string())?;

        // let public_key = untrusted::Input::from(public_key_bytes);
        // let msg = untrusted::Input::from(data_to_verify);
        // let sig = untrusted::Input::from(signature);

        let unparsed_pkey = signature::UnparsedPublicKey::new(&signature::ED25519, public_key_bytes);

        // match signature::verify(&signature::ED25519, public_key, msg, sig) {
        // match public_key.verify(msg, sig) {
        match unparsed_pkey.verify(data_to_verify, signature) {
            Ok(_) => Ok(true),
            Err(_) => Ok(false)
        }
    }

    pub fn pk_sign_file<T: AsRef<Path>>(&self, path: T) -> Result<Vec<u8>, String> {
        if path.as_ref().exists() {
            //open file
            let mut file = fs::File::open(path).map_err(|e|e.to_string())?;
            //must load the whole package file in RAM, could be a problem
            let mut file_in_ram: Vec<u8> = Vec::new();
            let _ = file.read_to_end(&mut file_in_ram).map_err(|e|e.to_string())?;

            self.pk_sign(&file_in_ram)
        }
        else {
            Err(format!("File cannot be signed: it does not exist."))
        }
    }

    pub fn pk_verify_file<T: AsRef<Path>>(public_key_bytes: &[u8], path: T, signature: &[u8]) -> Result<bool, String> {
        if path.as_ref().exists() {
            //open file
            let mut file = fs::File::open(path).map_err(|e|e.to_string())?;
            //must load the whole package file in RAM, could be a problem
            let mut file_in_ram: Vec<u8> = Vec::new();
            let _ = file.read_to_end(&mut file_in_ram).map_err(|e|e.to_string())?;

            KeyFile::pk_verify(public_key_bytes, &file_in_ram, signature)
        }
        else {
            Err(format!("File cannot be signed: it does not exist."))
        }
    }

    ///Get the public key for the pk signature verification
    pub fn get_pk_verification_key(&self) -> Result<Vec<u8>, String> {
        let key_pair = signature::Ed25519KeyPair::from_pkcs8(&self.pkcs8_key_pair).map_err(|e|e.to_string())?;
        let pub_key = key_pair.public_key();
        Ok(pub_key.as_ref().to_vec())
    }
}

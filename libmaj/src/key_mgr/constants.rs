// @Author: Lashermes Ronan <ronan>
// @Date:   07-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 07-07-2017
// @License: MIT

use ring::{digest, aead, pbkdf2};

pub static DIGEST_ALG: &'static digest::Algorithm = &digest::SHA256;
pub static PBKDF2_ALG: pbkdf2::Algorithm = pbkdf2::PBKDF2_HMAC_SHA256;
pub static AEAD_ALG: &'static aead::Algorithm = &aead::CHACHA20_POLY1305;
pub const CREDENTIAL_LEN: usize = digest::SHA256_OUTPUT_LEN;
pub const NONCE_LEN: usize = 12;//AEAD_ALG.nonce_len();
pub const SALT_LEN: usize = 16;
pub const AUTH: &str = "maj";
pub type Credential = [u8; CREDENTIAL_LEN];
pub const PBKDF2_ITER_NB: u32 = 100000;
pub const PAYLOAD_SIZE: usize = digest::SHA256_OUTPUT_LEN;


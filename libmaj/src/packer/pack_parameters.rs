// @Author: Lashermes Ronan <ronan>
// @Date:   07-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 07-07-2017
// @License: MIT

use std::path::{PathBuf, Path};
use crate::packer::AppData;

pub struct PackParameters {
    ///Path to where to output package file
    package_output_path: PathBuf,
    app_data: AppData,
    //Path to the key containing file
    // key_file_path: PathBuf,
    ///Keep the package as a dir (for file picking) additionnally to compressed archive
    keep_dir: bool,
    ///Path to Cargo.toml dir
    root_path: PathBuf
}

impl PackParameters {
    pub fn new(keep_dir: bool, app_data: AppData, package_output_path: PathBuf, root_path: PathBuf) -> PackParameters {
        PackParameters { keep_dir, app_data, package_output_path, root_path }
    }

    pub fn get_package_output_path(&self) -> &Path {
        &self.package_output_path
    }

    pub fn get_app_data(&self) -> &AppData {
        &self.app_data
    }

    pub fn get_keep_dir(&self) -> bool {
        self.keep_dir
    }

    // pub fn get_key_file_path(&self) -> &Path {
    //     &self.key_file_path
    // }

    pub fn get_root_path(&self) -> &Path {
        &self.root_path
    }
}

// @Author: Lashermes Ronan <ronan>
// @Date:   10-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 24-07-2017
// @License: MIT

use crate::packer::AppData;
use crate::key_mgr::KeyFile;
use std::path::{PathBuf, Path};


#[derive(Debug,Clone)]
pub struct ManifestParameters {
    ///pointer to key file providing crypto services
    key_file: KeyFile,
    ///Path where all package files can be accessed before archived
    tmp_dir_path: PathBuf,
    ///App data (version, name...)
    app_info: AppData
}

impl ManifestParameters {
    pub fn new(key_file: KeyFile, tmp_dir_path: PathBuf, app_info: AppData) -> ManifestParameters {
        ManifestParameters { key_file, tmp_dir_path, app_info }
    }

    pub fn get_key_file(&self) -> &KeyFile {
        &self.key_file
    }

    pub fn get_tmp_dir_path(&self) -> &Path {
        &self.tmp_dir_path
    }

    pub fn get_app_data(&self) -> &AppData {
        &self.app_info
    }
}

// @Author: Lashermes Ronan <ronan>
// @Date:   04-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 24-07-2017
// @License: MIT

use std::fs::File;
use std::fs;
use std::io::{BufWriter, BufReader};
use std::path::Path;
use std::collections::HashMap;

use serde_json;
use base64::{encode, decode};

use crate::packer::package_manifest::ManifestParameters;
use crate::packer::AppData;
use crate::key_mgr::hmac_helpers::*;

const HMAC_KEY: &str = "hmac_key";
const HMAC: &str = "hmac";

const NAME: &str = "name";
const VERSION: &str = "version";
const AUTHORS: &str = "authors";

#[derive(Debug,Clone,Serialize,Deserialize)]
pub struct ManifestNode {
    entry_name: String,
    data: String,
    children: Vec<ManifestNode>
}


impl ManifestNode {
    pub fn new_string(name: String, data: String) -> ManifestNode {
        ManifestNode { entry_name: name, data: data, children: Vec::new() }
    }

    pub fn new_vec(name: String, data: Vec<u8>) -> ManifestNode {
        ManifestNode { entry_name: name, data: encode(&data), children: Vec::new() }
    }

    pub fn new_empty(name: String) -> ManifestNode {
        ManifestNode { entry_name: name, data: String::new(), children: Vec::new() }
    }

    fn create_root(_: &ManifestParameters) -> Result<ManifestNode, String> {
        Ok(ManifestNode::new_empty("package".to_string()))
    }

    fn node_from_file_digest(file_path: &Path, hmac_key_bytes: &[u8]) -> Result<ManifestNode, String> {
        if file_path.is_dir() {
            return Err(format!("Trying to get hmac of {} which is a folder.", file_path.display()));
        }

        //sign file
        // let signature = key_file.hmac_sign(&file_in_ram);
        let signature = hmac_sign_file(hmac_key_bytes, file_path)?;

        let name = match file_path.file_name() {
            Some(s) => {
                s.to_str().unwrap_or("null").to_string()
            },
            None => String::from("null")
        };

        Ok(ManifestNode { entry_name: name, data: encode(&signature), children: Vec::new() })
    }

    pub fn get_hmac_key(&self) -> Option<Vec<u8>> {
        self.get_child_by_name(HMAC_KEY).and_then(|n|decode(&n.data).ok())
    }

    pub fn get_hmac_hashmap(&self) -> Result<HashMap<String, Vec<u8>>, String> { //<file name, hmac sig>
        let mut map: HashMap<String, Vec<u8>> = HashMap::new();

        let hmac_node = self.get_child_by_name(HMAC).ok_or(format!("No hmac signatures in the package."))?;
        for child in hmac_node.children.iter() {
            map.insert(child.entry_name.clone(), decode(&child.data).map_err(|_|format!("Cannot decode {}", child.data))? );
        }

        Ok(map)
    }

    pub fn get_app_data(&self) -> Result<AppData, String> {
        let name = self.get_child_by_name(NAME).ok_or(format!("No app name in manifest."))?.get_data();
        let version = self.get_child_by_name(VERSION).ok_or(format!("No app version in manifest."))?.get_data();
        let authors = self.get_child_by_name(AUTHORS).ok_or(format!("No app authors in manifest."))?.get_data();

        Ok(AppData::new(name.to_string(), authors.to_string(), version.to_string()))
    }

    pub fn create_package_manifest(parameters: &ManifestParameters)
    -> Result<ManifestNode, String> {
        //create root
        let mut root = ManifestNode::create_root(parameters)?;
        //add app data;
        root.add_child(ManifestNode::new_string(VERSION.to_string(), parameters.get_app_data().get_version().to_string()));
        root.add_child(ManifestNode::new_string(NAME.to_string(), parameters.get_app_data().get_app_name().to_string()));
        root.add_child(ManifestNode::new_string(AUTHORS.to_string(), parameters.get_app_data().get_authors().to_string()));

        //create hmac key
        let hmac_key = generate_random_hmac_key()?;

        //add hmac verification key
        root.add_child(ManifestNode::new_vec(HMAC_KEY.to_string(), hmac_key.clone()));

        let mut hmac_node = ManifestNode::new_empty(HMAC.to_string());
        //let build a node for all files in tmp
        let file_paths = fs::read_dir(parameters.get_tmp_dir_path()).map_err(|e|e.to_string())?;
        for path_res in file_paths {
            if let Ok(dir_entry) = path_res {
                let path = dir_entry.path();
                if path.is_file() {
                    hmac_node.add_child(ManifestNode::node_from_file_digest(&path, &hmac_key)?);
                }
                else {
                    return Err(format!("{} is a folder where it must be a file.", path.display()));
                }
            }
        }

        //add hmac node to root
        root.add_child(hmac_node);

        Ok(root)
    }

    pub fn add_child(&mut self, child: ManifestNode) {
        self.children.push(child);
    }

    pub fn get_child_by_name(&self, child_name: &str) -> Option<&ManifestNode> {
        for child in self.children.iter() {
            if child.entry_name == child_name {
                return Some(child);
            }
        }
        None
    }

    pub fn get_name(&self) -> &str {
        &self.entry_name
    }

    pub fn get_data(&self) -> &str {
        &self.data
    }

    pub fn open_from_file<T: AsRef<Path>>(path: T) -> Result<ManifestNode, String> {
        let file = File::open(path).map_err(|e|e.to_string())?;
        let reader = BufReader::new(file);
        let manifest_node: ManifestNode = serde_json::from_reader(reader).map_err(|e|e.to_string())?;
        Ok(manifest_node)
    }

    pub fn save_to_file<T: AsRef<Path>>(&self, path: T) -> Result<(), String> {
        let file = File::create(path).map_err(|e|e.to_string())?;
        let writer = BufWriter::new(file);
        serde_json::to_writer(writer, &self).map_err(|e|e.to_string())?;
        Ok(())
    }
}

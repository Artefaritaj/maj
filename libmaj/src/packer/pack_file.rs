// @Author: Lashermes Ronan <ronan>
// @Date:   04-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 04-07-2017
// @License: MIT

use std::path::Path;
use std::io::{BufReader, BufRead, Read};
use std::fs::File;

use crate::packer::PackAction;

#[derive(Debug,Clone,Serialize,Deserialize)]
pub struct PackFile {
    actions: Vec<PackAction>
}

impl PackFile {
    pub fn new() -> PackFile {
            PackFile { actions: Vec::new() }
    }

    pub fn parse_reader<T: Read>(reader: T) -> Result<PackFile, String> {
        let file = BufReader::new(reader);
        let mut actions: Vec<PackAction> = Vec::new();

        for line in file.lines() {
            match line {
                Ok(l) => {
                    if l.starts_with("#") {//comment
                        continue;
                    }
                    else {
                        actions.push(PackAction::parse(&l)?);
                    }
                },
                _ => {}
            }

        }

        Ok(PackFile { actions })
    }

    pub fn parse_file<T: AsRef<Path>>(path: T) -> Result<PackFile, String> {
        let f = File::open(path).map_err(|e|e.to_string())?;
        PackFile::parse_reader(f)
    }

    pub fn get_actions(&self) -> &Vec<PackAction> {
        &self.actions
    }

    pub fn add_action(&mut self, new_action: PackAction) {
        self.actions.push(new_action);
    }

    pub fn add_actions(&mut self, mut new_actions: Vec<PackAction>) {
        self.actions.append(&mut new_actions);
    }

}

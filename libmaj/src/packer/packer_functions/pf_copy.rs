// @Author: Lashermes Ronan <ronan>
// @Date:   04-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 25-07-2017
// @License: MIT

use crate::packer::{
    PackFunction, 
    PackAction,
    packer_functions::{
        packer_function::*,
        ActionType,
        ActionContext
    }
};
use crate::helpers::apply_variables;
use crate::errors::*;

use std::fs;
use std::fs::File;
use std::path::{PathBuf, Path};
use tar::{Builder, Archive};

use rand;
use rand::Rng;
use rand::distributions::Alphanumeric;

const TEMP_FILE_LEN: usize = 20;
const UNPACK_TAR: &str = ".to_unpack.tar";
const FILENAME: &str = "$FILENAME";

///PackFunction copy
#[derive(Debug)]
pub struct PfCopy {
    ///Copy from this path
    source_path: String,
    ///Copy to this path
    destination_path: String,
    ///Is the concerned file a user file? May be kept at install/uninstall.
    user: bool
}


impl PfCopy {
    pub fn from_args(args: &Vec<String>) -> Result<PfCopy> {
        if args.len() == 2 {
            Ok(PfCopy { source_path: args[0].clone(), destination_path: args[1].clone(), user: false })
        }
        else if args.len() == 3 {
            let user = if args[2] == USER {
                true
            }
            else { false };

            Ok(PfCopy { source_path: args[0].clone(), destination_path: args[1].clone(), user: user })
        }
        else {
            Err("copy function requires 2 or 3 arguments.".into())
        }
    }
}

impl PackFunction for PfCopy {
    fn exec(&self, ctx: &ActionContext, temp_dir: &Path) -> Result<Vec<PackAction>> {
        match ctx.get_action_type() {
            ActionType::Pack => self.pack_action(temp_dir, ctx),
            ActionType::Unpack => self.unpack_action(temp_dir, ctx),
            t => Err(format!("Action {:?} not supported.", t).into())
        }
    }
}

impl PfCopy {
    fn pack_action(&self, temp_dir: &Path, ctx: &ActionContext) -> Result<Vec<PackAction>> {
        let source = apply_variables(&self.source_path, ctx)?;

        info!("Copy {} -> {}, (user={})", source, self.destination_path, self.user);

        let path = Path::new(&source);
        let mut temp_file = temp_dir.to_path_buf();

        //generate a temp file path for the action to play with
        let mut temp_filename: String = rand::thread_rng()
                                    .sample_iter(&Alphanumeric)
                                    .take(TEMP_FILE_LEN)
                                    .collect();

        if path.is_dir() {//dir
            temp_filename += UNPACK_TAR;
            temp_file.push(&temp_filename);

            let file = File::create(&temp_file)
                .map_err(|e|format!("Cannot create {}: {}", temp_file.display(), e.to_string()))?;
            let mut archive = Builder::new(file);
            archive.append_dir_all("", path).map_err(|_|format!("Could not add directory {} to archive.", path.display()))?;
            archive.finish().map_err(|e|format!("Cannot create archive: {}", e.to_string()))?;
        }
        else {//file
            temp_file.push(&temp_filename);
            fs::copy(path, &temp_file).map_err(|e|format!("Cannot copy {} to {}: {}", path.display(), temp_file.display(), e.to_string()))?;
        }

        let src_filename = match path.file_name() {
            Some(s) => s.to_str().and_then(|s|Some(s.to_string())).unwrap_or(String::new()),
            None => String::new()
        };

        let mut pa = PackAction::new(COPY.to_string());
        pa.add_arg(format!("{}",temp_filename));
        pa.add_arg(format!("{}",PfCopy::var_filename(&self.destination_path, &src_filename)));

        if self.user == true {
            pa.add_arg(format!("{}",USER));
        }
        debug!("New install action: {:?}", pa);
        Ok(vec![pa])
    }

    ///Prepare the destination path if $FILENAME is used
    fn var_filename(dest: &str, file_name: &str) -> String {
        dest.replace(FILENAME, file_name)
    }

    fn unpack_action(&self, temp_dir: &Path, ctx: &ActionContext) -> Result<Vec<PackAction>> {
        let dest_str = apply_variables(&self.destination_path, ctx)?;
        let destination = PathBuf::from(&dest_str);
        let mut source = temp_dir.to_path_buf();
        source.push(&self.source_path);

        info!("Install action: copy {} -> {} (user={})", source.display(), destination.display(), self.user);

        if destination.exists() == true && self.user == true && ctx.get_keep_user_data() == true {//do not touch user data
            let mut pa = PackAction::new(DELETE.to_string());
            pa.add_arg(format!("{}", destination.display()));
            pa.add_arg(format!("{}", USER));
            Ok(vec![pa])
        }
        else {
            if self.source_path.ends_with(UNPACK_TAR) {//extract dir from tar
                let file = File::open(source)?;
                let mut archive = Archive::new(file);

                if let Some(parent_dir) = destination.parent() {
                    fs::create_dir_all(parent_dir)?;
                }

                archive.unpack(&destination)?;
                info!("Unpacking tar at {}", Path::new(&destination).display());
            }
            else {
                {//create dir hierarchy if needed
                    let parent_dir_opt = if destination.is_dir() {
                        Some(destination.as_path())
                    }
                    else {
                        destination.parent()
                    };

                    if let Some(parent_dir) = parent_dir_opt {
                        fs::create_dir_all(parent_dir)?;
                    }
                }

                //if dest is a dir, we need to add the filename for a successful copy
                // if destination.is_dir() {
                //     if let Some(filename) = source.file_name() {
                //         destination.push(filename);
                //     }
                // }

                fs::copy(&source, &destination)
                .map_err(|e|format!("Failed to copy {} to {}: {}", source.display(), destination.display(), e.to_string()))?;
                info!("Copying to {}", Path::new(&destination).display());
            }

            let mut pa = PackAction::new(DELETE.to_string());
            pa.add_arg(format!("{}", destination.display()));
            if self.user {
                pa.add_arg(format!("{}", USER));
            }
            debug!("New uninstall action: {:?}", pa);
            Ok(vec![pa])
        }
    }
}

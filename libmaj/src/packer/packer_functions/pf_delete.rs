// @Author: Lashermes Ronan <ronan>
// @Date:   18-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 25-07-2017
// @License: MIT

use crate::packer::{
    PackFunction, 
    PackAction,
    packer_functions::{
        packer_function::*,
        ActionType,
        ActionContext
    }
};
use crate::errors::*;

use std::path::Path;
use std::fs;

///PackFunction copy
#[derive(Debug)]
pub struct PfDelete {
    ///Copy to this path
    target_path: String,
    ///Is the concerned file a user file? May be kept at install/uninstall.
    user: bool
}


impl PfDelete {
    pub fn from_args(args: &Vec<String>) -> Result<PfDelete> {
        if args.len() == 1 {
            Ok(PfDelete { target_path: args[0].clone(), user: false })
        }
        else if args.len() == 2 {
            let user = if args[1] == USER {
                true
            }
            else { false };

            Ok(PfDelete { target_path: args[0].clone(), user: user })
        }
        else {
            Err("copy function requires 2 or 3 arguments.".into())
        }
    }
}

impl PackFunction for PfDelete {
    fn exec(&self, ctx: &ActionContext, _: &Path) -> Result<Vec<PackAction>> {
        match ctx.get_action_type() {
            ActionType::Uninstall => self.delete_action(ctx),
            t => Err(format!("Action {:?} not supported for delete.", t).into())
        }
    }
}

impl PfDelete {
    fn delete_action(&self, ctx: &ActionContext) -> Result<Vec<PackAction>> {
        if self.user == true && ctx.get_keep_user_data() == true {//keep user data if asked
            //do nothing
            info!("Keeping {} since we keep user data.", self.target_path);
            Ok(Vec::new())
        }
        else {
            let destination = Path::new(&self.target_path);
            info!("Deleting {}", destination.display());
            if destination.is_dir() {
                fs::remove_dir_all(destination)?;
            }
            else {
                fs::remove_file(destination)?;
            }
            Ok(Vec::new())
        }
    }
}

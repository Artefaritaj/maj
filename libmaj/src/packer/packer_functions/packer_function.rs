// @Author: Lashermes Ronan <ronan>
// @Date:   04-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 25-07-2017
// @License: MIT

use crate::packer::{
    PackAction,
    packer_functions::*
};
use crate::errors::*;

use std::path::Path;

//hard coded functions
pub const COPY: &str = "copy";
pub const DELETE: &str = "delete";
pub const ADD_SYMLINK: &str = "add_symlink";
pub const SYMLINK: &str = "symlink";
pub const DELETE_SYMLINK: &str = "delete_symlink";

pub const USER: &str = "user";

pub trait PackFunction {
    ///execute action and create next action.
    // pack -> unpack -> uninstall
    fn exec(&self, ctx: &ActionContext, temp_dir: &Path) -> Result<Vec<PackAction>>;
}

pub fn action_builder(pack_action: &PackAction) -> Result<Box<dyn PackFunction>> {
    match pack_action.get_action() {
        COPY => Ok(Box::new(PfCopy::from_args(pack_action.get_args())?)),
        DELETE => Ok(Box::new(PfDelete::from_args(pack_action.get_args())?)),
        ADD_SYMLINK | SYMLINK => Ok(Box::new(PfSymlinkAdd::from_args(pack_action.get_args())?)),
        DELETE_SYMLINK => Ok(Box::new(PfSymlinkDel::from_args(pack_action.get_args())?)),
        a => Err(format!("Unkown action: {}", a).into())
    }
}

#[test]
fn test_trait_obj() {
    let _: Option<Box<dyn PackFunction>> = None;
}

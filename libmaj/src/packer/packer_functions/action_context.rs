// @Author: Lashermes Ronan <ronan>
// @Date:   13-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 13-07-2017
// @License: MIT

use crate::app_dirs::OwnedAppInfo;

use std::path::Path;

#[derive(Debug,Clone,Copy,Serialize,Deserialize)]
pub enum ActionType {
    Pack,
    Unpack,//=install
    Uninstall
}

pub struct ActionContext<'a> {
    action_type: ActionType,
    ///Data about the packaged app
    app_info: &'a OwnedAppInfo,
    ///The path of the Cargo.toml dir
    root_path: &'a Path,
    ///Delete or keep user data at install and uninstall?
    keep_user_data: bool
}

impl<'a> ActionContext<'a> {
    pub fn new(action_type: ActionType, app_info: &'a OwnedAppInfo, root_path: &'a Path, keep_user_data: bool) -> ActionContext<'a> {
        ActionContext { action_type, app_info, root_path, keep_user_data }
    }

    pub fn get_action_type(&self) -> ActionType {
        self.action_type
    }

    pub fn get_app_info(&'a self) -> &'a OwnedAppInfo {
        &self.app_info
    }

    pub fn get_root_path(&'a self) -> &'a Path {
        &self.root_path
    }

    pub fn get_keep_user_data(&'a self) -> bool {
        self.keep_user_data
    }
}

// @Author: Lashermes Ronan <ronan>
// @Date:   04-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 25-07-2017
// @License: MIT

use crate::packer::{
    PackFunction, 
    PackAction,
    packer_functions::{
        packer_function::*,
        ActionType,
        ActionContext
    }
};
use crate::helpers::apply_variables;
use crate::errors::*;

use std::path::{PathBuf, Path};

use symlink::symlink_auto;


///PackFunction add symlink
#[derive(Debug)]
pub struct PfSymlinkAdd {
    ///Symlink location
    location_path: String,
    ///Symlink point to this location
    point_to_path: String,
    ///Is the concerned file a user file? May be kept at install/uninstall.
    user: bool
}


impl PfSymlinkAdd {
    pub fn from_args(args: &Vec<String>) -> Result<PfSymlinkAdd> {
        if args.len() == 2 {
            Ok(PfSymlinkAdd { location_path: args[0].clone(), point_to_path: args[1].clone(), user: false })
        }
        else if args.len() == 3 {
            let user = if args[2] == USER {
                true
            }
            else { false };

            Ok(PfSymlinkAdd { location_path: args[0].clone(), point_to_path: args[1].clone(), user: user })
        }
        else {
            Err("symlink function requires 2 or 3 arguments.".into())
        }
    }
}

impl PackFunction for PfSymlinkAdd {
    fn exec(&self, ctx: &ActionContext, temp_dir: &Path) -> Result<Vec<PackAction>> {
        match ctx.get_action_type() {
            ActionType::Pack => self.pack_action(temp_dir, ctx),
            ActionType::Unpack => self.unpack_action(temp_dir, ctx),
            t => Err(format!("Action {:?} not supported.", t).into())
        }
    }
}

impl PfSymlinkAdd {
    //at pack, just prepare install action
    fn pack_action(&self, _: &Path, _: &ActionContext) -> Result<Vec<PackAction>> {
        info!("Symlink {} -> {}, (user={})", self.location_path, self.point_to_path, self.user);

        let mut pa = PackAction::new(ADD_SYMLINK.to_string());
        pa.add_arg(format!("{}",self.location_path));
        pa.add_arg(format!("{}",self.point_to_path));

        if self.user == true {
            pa.add_arg(format!("{}",USER));
        }
        debug!("New add_symlink action: {:?}", pa);
        Ok(vec![pa])
    }


    //at install, create symlink
    fn unpack_action(&self, _: &Path, ctx: &ActionContext) -> Result<Vec<PackAction>> {
        let point_str = apply_variables(&self.point_to_path, ctx)?;
        let loc_str = apply_variables(&self.location_path, ctx)?;
        let point = PathBuf::from(&point_str);
        let loc = PathBuf::from(&loc_str);

        info!("Install action: add simlink {} -> {} (user={})", loc.display(), point.display(), self.user);

        if loc.exists() == true && self.user == true && ctx.get_keep_user_data() == true {//do not touch user data
            let mut pa = PackAction::new(DELETE_SYMLINK.to_string());
            pa.add_arg(format!("{}", loc.display()));
            pa.add_arg(format!("{}", USER));
            Ok(vec![pa])
        }
        else {
            symlink_auto(&point, &loc)?;

            let mut pa = PackAction::new(DELETE_SYMLINK.to_string());
            pa.add_arg(format!("{}", loc.display()));
            debug!("New uninstall action: {:?}", pa);
            Ok(vec![pa])
        }
    }
}

// @Author: Lashermes Ronan <ronan>
// @Date:   04-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 25-07-2017
// @License: MIT

use crate::packer::{
    PackFunction, 
    PackAction,
    packer_functions::{
        packer_function::*,
        ActionType,
        ActionContext
    }
};
use crate::errors::*;

use std::path::{Path};

use symlink::remove_symlink_auto;


///PackFunction add symlink
#[derive(Debug)]
pub struct PfSymlinkDel {
    ///Symlink location
    location_path: String,
    ///Is the concerned file a user file? May be kept at install/uninstall.
    user: bool
}


impl PfSymlinkDel {
    pub fn from_args(args: &Vec<String>) -> Result<PfSymlinkDel> {
        if args.len() == 2 {
            Ok(PfSymlinkDel { location_path: args[0].clone(), user: false })
        }
        else if args.len() == 2 {
            let user = if args[1] == USER {
                true
            }
            else { false };

            Ok(PfSymlinkDel { location_path: args[0].clone(), user: user })
        }
        else {
            Err("delete symlink function requires 1 or 2 arguments.".into())
        }
    }
}

impl PackFunction for PfSymlinkDel {
    fn exec(&self, ctx: &ActionContext, _: &Path) -> Result<Vec<PackAction>> {
        match ctx.get_action_type() {
            ActionType::Uninstall => self.delete_action(ctx),
            t => Err(format!("Action {:?} not supported.", t).into())
        }
    }
}

impl PfSymlinkDel {

    fn delete_action(&self, ctx: &ActionContext) -> Result<Vec<PackAction>> {
        if self.user == true && ctx.get_keep_user_data() == true {//keep user data if asked
            //do nothing
            info!("Keeping {} since we keep user data.", self.location_path);
            Ok(Vec::new())
        }
        else {
            let destination = Path::new(&self.location_path);
            info!("Deleting {}", destination.display());
            remove_symlink_auto(destination)?;
            Ok(Vec::new())
        }
    }
}

// @Author: Lashermes Ronan <ronan>
// @Date:   06-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 06-07-2017
// @License: MIT

use crate::app_dirs::OwnedAppInfo;

#[derive(Debug,Clone)]
pub struct AppData {
    app_name: String,
    authors: String,
    version: String
} // waiting for app_dirs to release a new version

impl AppData {
    pub fn new(app_name: String, authors: String, version: String) -> AppData {
        AppData { app_name, authors, version }
    }

    pub fn get_app_name(&self) -> &str {
        &self.app_name
    }

    pub fn get_authors(&self) -> &str {
        &self.authors
    }

    pub fn get_version(&self) -> &str {
        &self.version
    }

    pub fn to_owned_app_info(&self) -> OwnedAppInfo {
        OwnedAppInfo { name: self.app_name.clone(), author: self.authors.clone() }
    }
}

// @Author: Lashermes Ronan <ronan>
// @Date:   05-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 26-07-2017
// @License: MIT

#[macro_use] extern crate nom;
extern crate tempdir;
extern crate rand;
extern crate tar;
extern crate serde;
#[macro_use] extern crate serde_derive;
extern crate serde_json;
extern crate rayon;
extern crate xz2;
extern crate ring;
extern crate fs_extra;
// extern crate untrusted;
extern crate base64;
extern crate dirs;
// extern crate app_dirs;
#[macro_use] extern crate log;
#[macro_use] extern crate lazy_static;
extern crate file_fetcher;
extern crate semver;
#[macro_use]  extern crate error_chain;
extern crate symlink;

pub mod packer;
pub mod key_mgr;
pub mod version_mgr;
pub mod helpers;
pub mod errors;
pub mod updater;
pub mod app_dirs;

pub use self::packer::*;
pub use self::key_mgr::*;
pub use self::version_mgr::*;
pub use self::updater::Updater;
pub use self::app_dirs::*;

pub use file_fetcher::Url as Url;
pub use file_fetcher::UrlError as UrlError;
// @Author: Lashermes Ronan <ronan>
// @Date:   26-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 26-07-2017
// @License: MIT

use crate::Url;
use semver::{Version, VersionReq};

use crate::errors::*;
use crate::version_mgr::VersionManager;
use crate::packer::Packer;

pub struct Updater {}

impl Updater {

    pub fn update_str(version_endpoint: &str, pub_key: &[u8], this_version: &str, update_policy: &str) -> Result<Option<String>> {
        let version_endpoint = Url::parse(version_endpoint)?;
        let this_version = Version::parse(this_version)?;
        let update_policy = VersionReq::parse(update_policy)?;
        Updater::update(version_endpoint, pub_key, &this_version, &update_policy)
    }

    pub fn check_for_update_str(version_endpoint: &str, pub_key: &[u8], this_version: &str, update_policy: &str)
    -> Result<Option<String>> {
        let version_endpoint = Url::parse(version_endpoint)?;
        let this_version = Version::parse(this_version)?;
        let update_policy = VersionReq::parse(update_policy)?;
        Updater::check_for_update(version_endpoint, pub_key, &this_version, &update_policy)
    }

    pub fn check_for_update(version_endpoint: Url, pub_key: &[u8], this_version: &Version, update_policy: &VersionReq)
    -> Result<Option<String>> {
        //get version manager at endpoint
        let vmgr = VersionManager::from_url(version_endpoint)?;

        //verify signatures in version manager
        if vmgr.verify(&pub_key).map_err(|e|e.to_string())? == true {
            debug!("Url signatures verified.");
        }
        else {
            return Err("The versions file pointing to package url has been tempered with.".into());
        }

        //get new version url if any
        let last_entry = vmgr.get_last_matching_version(update_policy).ok_or(format!("No app with matching versions available."))?;

        if last_entry.get_version() <= this_version {
            Ok(None)
        }
        else {//a new version is available
            Ok(Some(last_entry.get_version().to_string()))
        }
    }

    pub fn update(version_endpoint: Url, pub_key: &[u8], this_version: &Version, update_policy: &VersionReq) -> Result<Option<String>> {
        info!("Trying to update package at {}", version_endpoint);
        //get version manager at endpoint
        let vmgr = VersionManager::from_url(version_endpoint)?;

        //verify signatures in version manager
        if vmgr.verify(&pub_key).map_err(|e|e.to_string())? == true {
            info!("Url signatures verified.");
        }
        else {
            return Err("The versions file pointing to package url has been tempered with.".into());
        }

        //get new version url if any
        let last_entry = vmgr.get_last_matching_version(update_policy).ok_or(format!("No app with matching versions available."))?;

        if last_entry.get_version() <= this_version {
            info!("The app is up-to-date.");
            Ok(None)
        }
        else {//a new version is available
            info!("Updating {} from version {} to {}", vmgr.get_app_name(), this_version, last_entry.get_version());
            //uninstall current version
            info!("Uninstalling {} {}", vmgr.get_app_name(), this_version);
            Packer::uninstall(vmgr.get_app_name(), true)?;//WARNING: if fail here, the app may not be installed anymore
            //install new version
            info!("Installing {} {}", vmgr.get_app_name(), last_entry.get_version());
            Packer::install_remote_package(last_entry.get_url(), pub_key, true)?;
            info!("Update successfuly applied. Restart your application to see changes.");
            Ok(Some(last_entry.get_version().to_string()))
        }
    }
}


#[test]
fn test_semver_parsing() {
    assert!(Version::parse("1").is_ok());
    assert!(Version::parse("0.1").is_ok());
    assert!(Version::parse("0.2.4").is_ok());
}
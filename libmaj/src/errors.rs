// @Author: Lashermes Ronan <ronan>
// @Date:   25-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 26-07-2017
// @License: MIT



error_chain! {
    foreign_links {
       Fmt(::std::fmt::Error);
       Io(::std::io::Error);
       SerdeJson(::serde_json::Error);
       AppDirs(crate::app_dirs::AppDirsError);
       FileFetcher(::file_fetcher::errors::Error);
       FsExtra(::fs_extra::error::Error);
       SemVer(::semver::SemVerError);
       SemVerReq(::semver::ReqParseError);
       Url(crate::UrlError);
   }

   // errors {
   //     ProtocolNotSupported(protocol: String) {
   //         description("protocol not supported")
   //         display("protocol {} is not supported", protocol)
   //     }
   // }
}

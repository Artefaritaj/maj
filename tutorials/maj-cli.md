# maj-cli

Packing your application is required before installations or updates. A package is the payload in the app distribution process.

Packaging an application requires a private key whose secrecy is necessary for the security of the scheme. As a result, **it is not and will never be possible to automatically package an application with your favorite CI**.
Otherwise it would be at the expense of the authenticity and integrity guarantees.

## Install the maj-cli utility application

Just a
```
cargo install cargo-maj
```
away.

## Create the private key

To ensure the security of the installation process you, as the application **provider**, need to have a public/private key pair.
To create a new private key (at default location), use the *create-private-key* subcommand.
```
cargo-maj create-private-key
```
Follow the indications to create your private key file.
If you loose the key file or the protecting password, you won't be able to generate new packages with this key. This normally requires to ship new installers or updater components to your users.

The file actually stores the public key as well as the private key and the hmac key.

To show the public key:
```
cargo-maj show-public-key
```

## Create a package

For now, package creation requires a *Cargo.toml* file.
At least one "pack data file" must be present and pointed to in *Cargo.toml*.

```TOML
[package.metadata.maj]
pack_data_files = ["install_instructions.txt"]
```

Now the package can be created with
```TOML
cargo-maj pack
```
if a private key file is present at default location.

## Add a package to version tracking file

Require a private key.
If the package is available over http(s) at *url1*.
```TOML
cargo-maj add-version url1
```
The package will be downloaded and its metadata (version + name) included in the *versions.json* tracking file.

If the url is not reachable but the package is available locally at *path1*.

```TOML
cargo-maj add-version url1 -i path1
```

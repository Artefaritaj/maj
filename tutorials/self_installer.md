# Use-case: self contained installer

The scenario is:
- you want to ship your target app (called *target_app*) and resources present in folder at path *PATH_TO_DIR*.
- For that a self contained installer is provided to your users.
- Upon execution, your app *target_app* is installed at a proper location, as well as the resources data.
- An uninstaller is created in the process.

Check the *examples* dir for the complete sources.

## Preparing yourself

Install the maj CLI tool and create your own private key as shown in the [maj CLI tutorial](./maj-cli.md).

## Packing *target_app*

The installer is relative to a target.
To prepare the target, modify its **Cargo.toml** file.
Add
```TOML
[package.metadata.maj]
pack_data_files = ["install_instructions.txt"]
#key_file = "path_to_key_file" #if not default
```

*install_instructions.txt* is a file path relative to Cargo.toml.
It contains a description of the actions to perform during the installation.

*pack_data_files* can contain several file paths. All files are executed in parallel at installation, in each file the instructions are executed sequentially.

Let's take a look at *install_instructions.txt*.
```txt
# line is comment if # is 1st character in the line
copy "$BINARY" "$SHARED_DATA/$FILENAME"
copy "resources/user" "$USER_CONFIG" "user"
copy "resources/shared" "$SHARED_CONFIG"
```

That is all, everything is ready.
Create your package with the maj CLI utility.

```
cargo maj pack
```

*target_app* is now ready to be deployed.

## The installer app *target_install*

Let's build our installer app called *target_install*.
```
cargo new target_install --bin
```

Add the following dependencies:
```TOML
[dependencies]
libmaj = { path = "../../libmaj" } #TODO
base64 = "0.6"
```

Then fill the main.rs file:
```rust
extern crate libmaj;
extern crate base64;

use libmaj::Packer;
use base64::decode;

fn main() {
    //The target package is included **at compilation** in the installer here
    let app_package = include_bytes!("../../target_app/target_app.0.1.0.package.tar.xz");
    //The public key to verify the package (base64 encoded)
    let public_key = "EXeczPQRpACoNE5Q1mnQeay2ZOK7RyW3DNEnElCO3ho=";

    match run(app_package, public_key) {
        Ok(_) => {},
        Err(e) => {println!("Application error: {}", e.to_string()); }
    }
}

fn run(app_package: &[u8], public_key_str: &str) -> Result<(), String> {
    //get public key bytes
    let public_key_bytes = decode(public_key_str).map_err(|e|e.to_string())?;
    //install target_app
    Packer::install(app_package, &public_key_bytes, true)
}
```

# Use-case: auto-updater

Here we want our app to update itself if a new version is available.

## Version tracking

The version tracking is done with the help of a *versions.json* file.
This file contains entries with *version*, *package url* and *entry signature*.

Add a new package to the local *versions.json* file with
```TOML
cargo maj add-version -u url_to_this_package -i path_to_this_package_if_not_available_online_yet
```

## Example

To be able to self update, your application need a few hardcoded data:
- the endpoint is the url to the *versions.json* file.
- The public key to verify urls and packages.
- The update policy: use semver to decide to update or not. E.g. "~0" will update to the last "0.x.x" version.
- This package version.

An [example](../examples/target_app/src/main.rs):
```rust
const ENDPOINT: &str = "https://gitlab.com/Artefaritaj/maj/raw/master/examples/target_app/versions.json";
const PUB_KEY_B64: &str = "EXeczPQRpACoNE5Q1mnQeay2ZOK7RyW3DNEnElCO3ho=";
const UPDATE_POLICY: &str = "~0";
const THIS_VERSION: &str = env!("CARGO_PKG_VERSION");
```

To update this package:
```rust
let pub_key = base64::decode(PUB_KEY_B64)?;
let updated = libmaj::Updater::update_str(ENDPOINT, &pub_key, THIS_VERSION, UPDATE_POLICY)?;
```
And that's it.

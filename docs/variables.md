# Variables

Variables are special **$STRING** used in pack instruction files.
The authorized variables depends on the actual phase: *Pack* or *Install*.

## Common variables

- "$HOME" point to current HOME dir.
- "$APP_NAME" the name of the packaged app.

## Pack variables

- "$BINARY" or "$RELEASE_BINARY" the path to the app release binary.
- "$DEBUG_BINARY" the path to the app debug binary.

## Install variables

Use app_dirs folders:
- "$USER_DATA"
- "$USER_CONFIG"
- "$USER_CACHE"
- "$SHARED_DATA"
- "$SHARED_CONFIG"

## Special

- "$FILENAME" is authorized in copy's destination argument.
Examples:
```
copy "$BINARY" "$USER_DATA/$FILENAME"
copy "some_dir/some_file" "$USER_CONFIG/$FILENAME"
```

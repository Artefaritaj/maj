# Actions

## Pack instruction files

Actions are called in pack instruction files.
Before packing, these actions are put into files pointed to in *Cargo.toml*.
```TOML
[package.metadata.maj]
pack_data_files = ["pack_instruction_file1.txt"]
```

Here is an example of such a file:

```
# comment if # is 1st character in the line
copy "$BINARY" "$USER_DATA/$FILENAME"
copy "resources/user" "$USER_CONFIG" "user"
copy "resources/shared" "$USER_CONFIG"
```

Each line is an action.
Actions are executed sequentially in a same file. Actions in different files are executed in parallel.
A line is a comment if the first character in the line is '#'.
[Variables](./variables.md) may be used in actions arguments.

For now (libmaj 0.1.3), authorized actions are:
- copy,
- delete
- add_symlink (or just symlink),
- del_symlink

Arguments must be written between quotes: "arg".

Instructions are executed at each "phase": pack, install and uninstall.
Instructions at one phase self create new instructions for the next phases.
So pack instructions are automatically derived in install and uninstall instructions. 

## Copy

```
copy "from path" "to path" "user"
```

- "from path" where to find the file/folder **on the packer computer** (pack phase).
- "to path" where to write the file/folder **on the user computer** (install phase).
- "user" (optional) indicate that this file/folder is a user saved data. Mark a file/folder with "user" if you want to keep it untouched at update.

A copy pack instruction will generate the corresponding delete uninstall instruction.

## Delete

```
delete "path" "user"
```

- "path" file to delete (uninstall phase only).
- "user" (optional) indicate that this file/folder is a user saved data. File/folder will actually be deleted only if "user" is not present or "user" is present and "keep_user_data" is false.

## Add symlink

```
add_symlink "symlink file location" "point to path" "user"
```

Use case: add a symlink between /usr/bin and your app to be able to self update without admin rights.

## Delete symlink

```
del_symlink "symlink file location" "user"
```

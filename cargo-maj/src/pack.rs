// @Author: Lashermes Ronan <ronan>
// @Date:   22-06-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 26-07-2017
// @License: MIT


use std::process::Command;
use std::fs::File;
use std::io::Read;
use std::path::{PathBuf, Path};

use json;
use json::JsonValue;

use toml::Value as Toml;
use toml;

use crate::maj_config::{MajConfig, MajConfigParsed};
use libmaj::{PackFile, Packer, AppData, PackParameters, KeyFile};
use crate::errors::*;

///Create a package from command inputs
pub fn pack(out_pathbuf: PathBuf, key_file: KeyFile) -> Result<()> {
    //parse app to pack manifest to extract relevant data
    let manifest = read_cargo_manifest()?;
    let cargo_toml_str = manifest["manifest_path"].to_string();
    let parsed_maj_config = read_cargo_toml(&cargo_toml_str)?;
    let app_name = manifest["name"].to_string();
    let authors = manifest["authors"].to_string();
    let version = manifest["version"].to_string();
    let app_data = AppData::new(app_name, authors, version);
    let maj_config = MajConfig::from_parsed(parsed_maj_config, app_data.clone())?;

    // println!("Cargo manifest: {:?}", manifest);
    // println!("Config: {:?}", maj_config);

    let cargo_toml_path = Path::new(&cargo_toml_str);
    let root_path = cargo_toml_path.parent().ok_or(format!("No parent dir for {}", cargo_toml_str))?;

    //regroup parameters
    let pack_params = PackParameters::new(false, app_data, out_pathbuf, root_path.to_path_buf());

    //parse pack_files
    let mut pack_files: Vec<PackFile> = Vec::new();
    for file_path in maj_config.get_pack_data_files() {
        pack_files.push(PackFile::parse_file(file_path)?);
    }

    //interpret packing instructions in pack files
    let mut packer = Packer::from_files(pack_files);
    //create package
    packer.create_package(&pack_params, key_file)?;
    Ok(())
}

fn read_cargo_manifest() -> Result<JsonValue> {
    let json_manifest_bytes = Command::new("cargo").arg("read-manifest").output().map_err(|e|e.to_string())?;

    if json_manifest_bytes.status.success() == false {
        return Err("Failed to read cargo manifest.".into());
    }

    let json_manifest = String::from_utf8(json_manifest_bytes.stdout).unwrap_or("".to_string());
    Ok(json::parse(&json_manifest)?)
}

fn read_cargo_toml(cargo_toml_path: &str) -> Result<MajConfigParsed> {
    let mut file = File::open(cargo_toml_path).map_err(|e|e.to_string())?;
    let mut content = String::new();
    file.read_to_string(&mut content).map_err(|e|e.to_string())?;

    let toml = content.parse::<Toml>().map_err(|_|format!("Failed to read Cargo.toml."))?;
    let maj_config_toml = toml.get("package")
                            .and_then(|t|t.get("metadata"))
                            .and_then(|t|t.get("maj"))
                            .ok_or(format!("No maj config in Cargo.toml."))?;

    let maj_config: MajConfigParsed = toml::from_str(&maj_config_toml.to_string()).map_err(|e|e.to_string())?;

    Ok(maj_config)
}

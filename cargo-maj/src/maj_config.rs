// @Author: Lashermes Ronan <ronan>
// @Date:   04-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 04-07-2017
// @License: MIT


use std::path::{PathBuf, Path};

use crate::helpers::get_default_key_filepath;

use libmaj::AppData;


#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct MajConfigParsed {
    pack_data_files: Option<Vec<String>>,
    key_file: Option<String>,
}

#[derive(Debug)]
pub struct MajConfig {
    ///Info about the app to pack
    app_info: AppData,
    ///Files describing how to pack
    pack_data_files: Vec<String>,
    key_file: PathBuf,
}

impl MajConfig {
    pub fn from_parsed(parsed: MajConfigParsed, app_data: AppData) -> Result<MajConfig, String> {
        let mut pack_data_files: Vec<String> = Vec::new();
        if let Some(mut data) = parsed.pack_data_files {
            pack_data_files.append(&mut data);
        }

        let priv_file = if let Some(priv_key) = parsed.key_file {
            PathBuf::from(priv_key)
        }
        else {
            get_default_key_filepath()?
        };


        Ok(MajConfig { app_info: app_data, pack_data_files: pack_data_files, key_file: priv_file })
    }

    pub fn get_app_data(&self) -> &AppData {
        &self.app_info
    }

    pub fn get_pack_data_files(&self) -> &Vec<String> {
        &self.pack_data_files
    }

    pub fn get_key_file(&self) -> &Path {
        &self.key_file
    }
}

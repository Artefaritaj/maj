// @Author: Lashermes Ronan <ronan>
// @Date:   20-06-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 26-07-2017
// @License: MIT

#[macro_use] extern crate clap;
extern crate app_dirs;
extern crate rpassword;
extern crate json;
extern crate toml;
extern crate libmaj;
extern crate serde;
#[macro_use] extern crate serde_derive;
extern crate base64;
extern crate fern;
extern crate log;
extern crate chrono;
#[macro_use] extern crate error_chain;

pub mod helpers;
pub mod maj_config;
pub mod pack;
pub mod errors;

use clap::{App, ArgMatches};
use base64::{decode, encode};
use chrono::Local;
use errors::*;

use std::path::PathBuf;
use std::io;

use libmaj::{KeyFile, EncryptedKeyFile, Packer, VersionManager};
use pack::pack;
use helpers::{get_default_key_filepath};

const VERSIONS_FILE: &str = "versions.json";

fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml).get_matches();

    if let Err(e) = run_main(matches) {
        println!("Application error: {}", e);
    }
}

fn run_main(matches: ArgMatches) -> Result<()> {
    //get verbosity
    // Vary the output based on how many times the user used the "verbose" flag
    // (i.e. 'myprog -v -v -v' or 'myprog -vvv' vs 'myprog -v'
    let verbosity = matches.occurrences_of("verbose");

    prepare_log(verbosity)?;

    //manage create-private-key subcommands
    if let Some(matches) = matches.subcommand_matches("create-private-key") {
        create_key(&matches)?;
    }
    else if let Some(matches) = matches.subcommand_matches("install") {
        unpack_match(&matches)?;
    }
    else if let Some(matches) = matches.subcommand_matches("pack"){
        pack_match(&matches)?;
    }
    else if let Some(matches) = matches.subcommand_matches("show-public-key") {
        show_pub_key(&matches)?;
    }
    else if let Some(matches) = matches.subcommand_matches("uninstall") {
        uninstall_match(&matches)?;
    }
    else if let Some(matches) = matches.subcommand_matches("add-version") {
        add_version(&matches)?;
    }
    else if let Some(matches) = matches.subcommand_matches("ditch-version") {
        remove_version(&matches)?;
    }

    Ok(())
}

fn pack_match(matches: &ArgMatches) -> Result<()> {
    //where to store package?
    let out_pathbuf = if let Some(out_path) = matches.value_of("output") {
        PathBuf::from(out_path.to_string())
    }
    else {//default dir
        PathBuf::from("./")
    };

    //where to find key?
    let key_pathbuf = if let Some(key_path) = matches.value_of("key-path") {
        PathBuf::from(key_path.to_string())
    }
    else {//default dir
        get_default_key_filepath()?
    };

    //ask for password
    println!("Please enter the password for the key file: ");
    let pass: String = rpassword::read_password().map_err(|e|e.to_string())?;

    //load key file
    let key_file = (EncryptedKeyFile::from_file(&key_pathbuf)?).decrypt(&pass)?;

    pack(out_pathbuf, key_file)?;

    Ok(())
}

fn unpack_match(matches: &ArgMatches) -> Result<()> {
    //where to find package?
    let input = if let Some(in_path) = matches.value_of("input") {
        in_path.to_string()
    }
    else {//default dir
        String::from("./")
    };

    let clean_user_data = matches.is_present("clean");
    let keep_user_data = !clean_user_data;

    //where to find key?
    let key = if let Some(key_val) = matches.value_of("key-value") {
        decode(key_val).map_err(|_|format!("Cannot decode public key base64 string."))?
    }
    else {
        let key_pathbuf = if let Some(key_path) = matches.value_of("key-file") {
            PathBuf::from(key_path.to_string())
        }
        else {//default dir
            get_default_key_filepath()?
        };

        //ask for password
        println!("Please enter the password for the key file: ");
        let pass: String = rpassword::read_password().map_err(|e|e.to_string())?;

        //load key file
        let key_file = (EncryptedKeyFile::from_file(&key_pathbuf)?).decrypt(&pass)?;
        key_file.get_pk_verification_key()?
    };

    if let Err(_) = Packer::install_local_package(&input, &key, keep_user_data) {//try locally
        Packer::install_remote_package(&input, &key, keep_user_data)?;//if not try remotely
    }


    Ok(())
}

fn uninstall_match(matches: &ArgMatches) -> Result<()> {
    let clean_user_data = matches.is_present("clean");
    let keep_user_data = !clean_user_data;

    let app_name = if let Some(app_name) = matches.value_of("application") {
        app_name
    }
    else {
        return Err("No application given to uninstall.".into());
    };

    Packer::uninstall(app_name, keep_user_data)?;

    Ok(())
}

fn add_version(matches: &ArgMatches) -> Result<()> {
    let key_pathbuf = if let Some(key_path) = matches.value_of("key-file") {
        PathBuf::from(key_path.to_string())
    }
    else {//default dir
        get_default_key_filepath()?
    };

    //ask for password
    println!("Please enter the password for the key file: ");
    let pass: String = rpassword::read_password().map_err(|e|e.to_string())?;

    //load key file
    let key_file = (EncryptedKeyFile::from_file(&key_pathbuf)?).decrypt(&pass)?;
    let pub_key = key_file.get_pk_verification_key()?;

    let url = matches.value_of("url").ok_or(format!("No url provided"))?;
    let path_opt = matches.value_of("input");

    let appdata = match path_opt {
        Some(path) => {
            Packer::get_local_appdata(path, &pub_key)?
        },
        None => {
            Packer::get_remote_appdata(url, &pub_key)?
        }
    };

    //load last version file or create new
    let mut vmgr = VersionManager::from_file(VERSIONS_FILE)
                    .unwrap_or(VersionManager::new(appdata.get_app_name().to_string()));//create new if does not exist

    //add this version
    vmgr.add_app_version_str(appdata.get_version(), url.to_string(), &key_file)?;

    //save new version file
    vmgr.save_to_file(VERSIONS_FILE)?;

    Ok(())
}

fn remove_version(matches: &ArgMatches) -> Result<()> {
    let version = matches.value_of("version").ok_or(format!("No version to delete given."))?;

    //load last version file
    let mut vmgr = VersionManager::from_file(VERSIONS_FILE).map_err(|_|format!("No versions file found."))?;
    //remove version
    vmgr.remove_version_str(&version)?;
    //save new version file
    vmgr.save_to_file(VERSIONS_FILE)?;

    Ok(())
}

fn show_pub_key(matches: &ArgMatches) -> Result<()> {

    let key_pathbuf = if let Some(key_path) = matches.value_of("key-file") {
        PathBuf::from(key_path.to_string())
    }
    else {//default dir
        get_default_key_filepath()?
    };

    //ask for password
    println!("Please enter the password for the key file: ");
    let pass: String = rpassword::read_password().map_err(|e|e.to_string())?;

    //load key file
    let key_file = (EncryptedKeyFile::from_file(&key_pathbuf)?)
                                    .decrypt(&pass)?;
    println!("Key file decrypted.");
    let pub_key = key_file.get_pk_verification_key()?;

    println!("{}", encode(&pub_key));

    Ok(())
}

fn prepare_log(verbosity: u64) -> Result<()> {
    //get verbosity
    let log_level = match verbosity {
        1 => log::LevelFilter::Error,
        2 => log::LevelFilter::Warn,
        3 => log::LevelFilter::Info,
        4 => log::LevelFilter::Debug,
        5 => log::LevelFilter::Trace,
        _ => log::LevelFilter::Off
    };

    //configure logger
    // let log_filename = format!("out.log");

    fern::Dispatch::new()
    .format(|out, message, record| {
        //out.finish(format_args!("{}[{}][{}] {}",
        out.finish(format_args!("{}[{}] {}",
            Local::now()
                .format("[%Y-%m-%d][%H:%M:%S]"),
            //record.target(),
            record.level(),
            message))
    })
    .level(log_level)
    .chain(std::io::stdout())
    // .chain(fern::log_file(log_filename).map_err(|e|e.to_string())?)
    .apply()
    .map_err(|e|e.to_string())?;

    Ok(())
}

fn create_key(matches: &ArgMatches) -> Result<()> {
    //where to store key?
    let key_pathbuf = if let Some(out_path) = matches.value_of("output") {
        PathBuf::from(out_path.to_string())
    }
    else {//default dir
        get_default_key_filepath()?
    };


    //check if output key file exists and if yes, ask the user if he want to erase it
    if key_pathbuf.exists() {
        println!("A key file already exists at {}, if you continue this file (and the private key in it) will be definitely lost.\nDo you wish to continue? (Y/n)", key_pathbuf.display());
        
        let mut answer = String::new();
        match io::stdin().read_line(&mut answer) {
            Ok(_) => {
                if answer.trim() != "Y" {
                    return Err(format!("File {} already exists. Aborting.", key_pathbuf.display()).into());
                }
            }
            Err(error) => println!("Cannot read stdin: {}", error),
        }

        
    }

    if let Some(dir) = key_pathbuf.parent() {
        std::fs::create_dir_all(dir).map_err(|e|format!("Cannot create folder: {}", e.to_string()))?;
    }

    //ask for password.
    println!("Your key file will be password protected. Please enter your password (8 chars minimum).");
    let pass: String = rpassword::read_password().map_err(|e|e.to_string())?;
    if pass.chars().count() < 8 {
        return Err("The password must have 8 characters or more.".into());
    }

    println!("Lets reenter the password to be sure, shall we?");
    let pass_verif: String = rpassword::read_password().map_err(|e|e.to_string())?;
    if pass != pass_verif {
        return Err("Password verification failed, please enter twice the same password.".into());
    }

    let kf = KeyFile::new()?;
    println!("Key file created.");
    let enc_kf = EncryptedKeyFile::encrypt(&kf, &pass)?;
    println!("Key file encrypted.");

    //write to file
    println!("Writing to {}", key_pathbuf.display());
    enc_kf.save_to_file(&key_pathbuf)?;

    Ok(())
}

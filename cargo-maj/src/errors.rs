// @Author: Lashermes Ronan <ronan>
// @Date:   26-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 26-07-2017
// @License: MIT




error_chain! {
    foreign_links {
       Fmt(::std::fmt::Error);
       Io(::std::io::Error);
       LibMaj(::libmaj::errors::Error);
       Json(::json::Error);
   }

   // errors {
   //     ProtocolNotSupported(protocol: String) {
   //         description("protocol not supported")
   //         display("protocol {} is not supported", protocol)
   //     }
   // }
}

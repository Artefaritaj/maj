// @Author: Lashermes Ronan <ronan>
// @Date:   23-06-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 23-06-2017
// @License: MIT

use std::path::PathBuf;

use app_dirs::*;
pub const MAJ_APP_INFO: AppInfo = AppInfo{name: "maj", author: "Artefaritaj"};

pub fn get_default_key_filepath() -> Result<PathBuf, String> {
    let mut pb = get_app_root(AppDataType::UserConfig, &MAJ_APP_INFO).map_err(|_|format!("No config dir found."))?;
    pb.push("key");
    Ok(pb)
}

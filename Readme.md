# maj

**Beta: not production ready.**
Tested on Ubuntu-linux only for now, but designed as a cross-platform library.

## maj CLI

Utility to create application packages, manage signature keys, version management and more.

## libmaj - The installer and updater library.

libmaj is a library to write application installers and (auto-)updaters.
It provides utilities to pack/install/uninstall an application, verify its integrity and authenticity, and fetch application packages over the internet.

**Disclaimer: an auto-updater is basically a backdoor you install on your users' computers. Please do it securely and responsibly. Report ASAP any vulnerability you can find in this library by opening an [issue](https://gitlab.com/Artefaritaj/maj/issues).**

**It is nearly always better to use the platform specific application distribution mechanism (e.g. .deb/PPA for Debian/Ubuntu), for a better user experience and to limit the security issues.**

But hey, don't run just yet. They are use-cases where this lib is useful ... I think.

## Tutorials

 - [How to use the maj CLI utility (key, package and versions management)](./tutorials/maj-cli.md)
 - [How to make a self installer](./tutorials/self_installer.md)
 - [How to make an auto-updater](./tutorials/autoupdater.md)

## Pack instruction files

 - [Actions](./docs/actions.md)
 - [Variables](./docs/variables.md)
